/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 11/06/19 10:24 AM
 */
package com.pedinel.kore.adapters

import com.pedinel.kore.adapters.Utils.setLayoutAnimation
import androidx.lifecycle.LifecycleOwner
import android.widget.BaseAdapter
import android.view.ViewGroup
import androidx.annotation.AnimRes
import androidx.lifecycle.LiveData
import androidx.databinding.ViewDataBinding
import com.pedinel.kore.viewmodels.ItemViewModel
import androidx.annotation.CallSuper
import android.annotation.SuppressLint
import android.database.DataSetObserver
import androidx.annotation.AnyThread
import android.util.SparseArray
import android.view.View
import android.widget.ListView
import androidx.annotation.MainThread
import com.pedinel.kore.adapters.Observer.BaseImpl
import java.lang.ref.Reference
import java.lang.ref.WeakReference
import java.util.*

abstract class Adapter<T, VH : Adapter.ViewHolder<T>> protected constructor(owner: LifecycleOwner) :
    BaseAdapter(), ReactiveAdapter<T> {
    /**
     * Indicates if the adapter has more than 1 view type
     */
    private val isMultiView: Boolean

    /**
     * Holds the attached Views
     */
    private val attaches: MutableList<ViewAttach> = ArrayList()

    /**
     * Operation Handler delegated
     */
    private val delegated: AdapterHandler<T>

    /**
     * Attaches the adapter into a [ViewGroup] if not already attached.
     *
     * @param view the [ViewGroup]
     */
    fun attach(view: ViewGroup) {
        setLayoutAnimation(view, onGetLayoutAnimation())
        if (view is ListView) {
            view.adapter = this
        } else {
            if (findAttach(view, false) != null) {
                //already attached
                return
            }
            val attach = ViewAttach(view)
            attaches.add(attach)
            attach.setAdapter(this)
        }
        view.tag = this
    }

    /**
     * Detaches the adapter from a [ViewGroup] if already attached.
     *
     * @param view the [ViewGroup]
     */
    fun detach(view: ViewGroup) {
        if (view is ListView) {
            view.adapter = null
        } else {
            val attach = findAttach(view, true)
            attach?.setAdapter(null)
        }
        view.tag = null
    }

    abstract fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH

    open fun onFiltered(items: MutableList<T>): MutableList<T> {
        return items
    }

    protected val owner: LifecycleOwner
        protected get() = delegated.owner!!

    open fun getItemId(item: T): Long {
        return -1
    }

    @AnimRes
    open fun onGetLayoutAnimation(): Int {
        return 0
    }

    open fun onPreUpdate(root: ViewGroup) {}
    open fun onPostUpdate(root: ViewGroup) {}

    private fun findAttach(what: ViewGroup, remove: Boolean): ViewAttach? {
        for (e in attaches) {
            if (e.getRoot() === what) {
                if (remove) {
                    attaches.remove(e)
                }
                return e
            }
        }
        return null
    }

    override fun <X : Collection<T>> setSource(source: LiveData<X>?) {
        delegated.setSource(source)
    }

    override fun setItems(newItems: Collection<T>) {
        delegated.setItems(newItems)
    }

    override fun hasStableIds(): Boolean {
        return true
    }

    override fun getFilter(): Filter<T>? {
        return delegated.filter
    }

    override fun getCount(): Int {
        return delegated.getItemCount()
    }

    override fun setFilter(filter: Filter<T>?) {
        delegated.setFilter(filter!!)
    }

    override fun getItem(position: Int): T {
        return delegated.getItemAt(position)
    }

    override fun getItemId(position: Int): Long {
        return delegated.getItemId(position)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val item = delegated.getItemAt(position)
        val viewType = if (isMultiView) getItemViewType(position) else 0
        var holder: ViewHolder<T>? = null

        val recycled = convertView?.tag as ViewHolder<T>?
        if (recycled?.viewType == viewType) {
            holder = recycled
        }
        if (holder == null) {
            holder = onCreateViewHolder(parent, viewType)
            holder.itemView().tag = holder
        }
        if (holder.item !== item) {
            holder.replaceItem(item)
        }
        return holder.itemView()
    }

    abstract class ViewHolder<T> protected constructor(
        protected val binding: ViewDataBinding,
        protected val viewModel: ItemViewModel<T>,
        owner: LifecycleOwner?
    ) {
        var item: T? = null

        val viewType = 0

        fun replaceItem(value: T) {
            if (item !== value) {
                item = value
                viewModel.setItem(value)
                onChanged(value)
            }
        }

        @CallSuper
        protected fun onChanged(item: T) {
            binding.invalidateAll()
        }

        fun itemView(): View {
            return binding.root
        }

        init {
            binding.lifecycleOwner = owner
        }
    }

    private class ViewAttach(container: ViewGroup) {
        private val root: Reference<ViewGroup>

        @SuppressLint("UseSparseArrays")
        private val viewTypeMap: MutableMap<Int, Int> = HashMap()
        private val observer: DataSetObserver
        private var adapter: Adapter<*, *>? = null
        private var animate = true

        @AnyThread
        fun getRoot(): ViewGroup? {
            return root.get()
        }

        @AnyThread
        private fun cacheCycle(
            adapter: Adapter<*, *>,
            children: List<View>
        ): SparseArray<LinkedList<View>> {
            val out = SparseArray<LinkedList<View>>(adapter.viewTypeCount)
            for (child in children) {
                val type = viewTypeMap[child.hashCode()] ?: continue
                var list = out[type]
                if (list == null) {
                    list = LinkedList()
                    out.put(type, list)
                }
                list.add(child)
            }
            viewTypeMap.clear()
            return out
        }

        @MainThread
        private fun updateViews(o: Adapter<*, *>) {
            val size = o.count
            val root = getRoot() ?: return
            o.onPreUpdate(root)
            val childList: MutableList<View> = ArrayList(root.childCount)
            for (i in root.childCount - 1 downTo 0) {
                childList.add(root.getChildAt(i))
            }
            val cache = cacheCycle(o, childList)
            childList.clear()
            for (i in 0 until size) {
                val viewType = o.getItemViewType(i)
                val cacheList = cache[viewType]
                var convertView =
                    if (cacheList == null || cacheList.isEmpty()) null else cacheList.removeFirst()
                convertView = o.getView(i, convertView, root)
                root.addView(convertView)
                viewTypeMap[convertView.hashCode()] = viewType
            }
            o.onPostUpdate(root)
            if (animate && size > 0) {
                root.scheduleLayoutAnimation()
                animate = false
            }
        }

        @MainThread
        private fun applyDataChanges() {
            val root = getRoot() ?: return
            root.removeAllViews()
            val o = adapter ?: return
            updateViews(o)
        }

        @MainThread
        fun setAdapter(adapter: Adapter<*, *>?) {
            if (this.adapter === adapter) return
            if (this.adapter != null) {
                this.adapter!!.unregisterDataSetObserver(observer)
            }
            this.adapter = adapter
            adapter?.registerDataSetObserver(observer)
            applyDataChanges()
        }

        init {
            root = WeakReference(container)
            observer = object : DataSetObserver() {
                override fun onChanged() {
                    applyDataChanges()
                }

                override fun onInvalidated() {
                    applyDataChanges()
                }
            }
        }
    }

    companion object {
        /**
         * Retrieves the [Adapter] attached to a [ViewGroup] (if any).
         *
         * @param view the [ViewGroup] that may have an attached [Adapter]
         * @param <T> the [Adapter] subclass type
         *
         * @return an attached [Adapter] object
        </T> */
        fun <T : Adapter<*, *>?> getAdapter(view: ViewGroup): T? {
            val tag = view.tag
            return if (tag is Adapter<*, *>) {
                tag as T
            } else null
        }
    }

    init {
        isMultiView = viewTypeCount > 1
        delegated = AdapterHandler(owner, BaseImpl(this))
    }
}