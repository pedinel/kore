/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 06/05/19 04:56 PM
 */
package com.pedinel.kore.adapters

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import java.lang.ref.Reference
import java.lang.ref.WeakReference
import java.util.ArrayList

/**
 * Helper class for Reactive adapter implementations
 * @param <T> the Adapter data type
</T> */
internal class AdapterHandler<T>(
    owner: LifecycleOwner,
    /**
     * The Adapter event notifier
     */
    private val notifier: Observer<T>
) {
    /**
     * The LifeCycleOwner that keeps the adapter alive
     */
    val ownerRef: Reference<LifecycleOwner>

    /**
     * The data filter
     */
    @JvmField
    var filter: Filter<T>? = null

    /**
     * The data source that updates the items
     */
    private var plugged: AttachWrapper<out Collection<T>, T>? = null

    /**
     * The items added to the adapter (raw source)
     */
    private var items: List<T> = ArrayList()

    /**
     * Displaying items
     */
    private var displayingItems = items
    fun setFilter(filter: Filter<T>) {
        if (this.filter === filter) return
        this.filter = filter
        buildDisplayingItems()
    }

    /**
     * Retrieves the [LifecycleOwner] where this [Adapter] belongs to
     *
     * @return a [LifecycleOwner] object, or `null` if the owner does not exists anymore
     */
    val owner: LifecycleOwner?
        get() = ownerRef.get()

    fun <X : Collection<T>> setSource(source: LiveData<X>?) {
        if (plugged?.source === source) return

        plugged?.unplug()
        plugged = null

        val owner = owner
        if (owner != null && source != null) {
            plugged = AttachWrapper(source, notifier.adapter()).apply {
                plug(owner)
            }
        }
    }

    fun setItems(newItems: Collection<T>?) {
        if (items.isEmpty()) {
            if (newItems == null || newItems.isEmpty()) {
                buildDisplayingItems()
                return
            }
        }

        //update is needed
        items = if (newItems == null) {
            ArrayList()
        } else {
            ArrayList(newItems)
        }
        buildDisplayingItems()
    }

    fun getItemCount(): Int {
        return displayingItems.size
    }

    fun getItemAt(position: Int): T {
        return displayingItems[position]
    }

    fun getItemId(position: Int): Long {
        val item = getItemAt(position)
        return notifier.getItemId(item)
    }

    private fun buildDisplayingItems() {
        val filter = filter
        if (filter == null) {
            onItemsChanged(items)
        } else {
            var newItems: MutableList<T> = ArrayList()
            for (item in items) {
                if (filter.applies(item)) {
                    newItems.add(item)
                }
            }
            newItems = notifier.onFiltered(newItems)
            onItemsChanged(newItems)
        }
    }

    private fun onItemsChanged(newItems: List<T>) {
        val oldItems = displayingItems
        displayingItems = newItems
        notifier.onDataSetChanged(oldItems, newItems)
    }

    init {
        ownerRef = WeakReference(owner)
    }
}