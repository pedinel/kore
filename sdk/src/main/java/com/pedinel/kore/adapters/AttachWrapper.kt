/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 16/04/19 02:23 PM
 */
package com.pedinel.kore.adapters

import android.os.Handler
import androidx.lifecycle.LiveData
import android.os.Looper
import android.os.SystemClock
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer

/**
 * Helper class to allow keeping a single source reference for [Adapter] or [RecyclerAdapter]
 *
 * @param <X> the [Collection] subclass
 * @param <T> the source data type
</T></X> */
internal class AttachWrapper<X : Collection<T>, T>(
    /**
     * The data source
     */
    val source: LiveData<X>, adapter: ReactiveAdapter<T>
) {
    /**
     * The observer that updates the items in the adapter
     */
    private val observer: Observer<X>
    private val handler = Handler(Looper.getMainLooper())
    private var itemsToUpdate: X? = null
    private var time: Long = 0
    internal var delayInMillis = 300

    /**
     * Attaches the observer into the adapter
     *
     * @param owner the [LifecycleOwner] to keep
     */
    fun plug(owner: LifecycleOwner?) {
        source.observe(owner!!, observer)
    }

    /**
     * Detaches the observer from the adapter. The source will not update the adapter anymore.
     * This is useful when a new source is attached.
     */
    fun unplug() {
        source.removeObserver(observer)
    }

    init {
        val updateTask = Runnable {
            val itemsToUpdate = this.itemsToUpdate
            if (itemsToUpdate != null) {
                adapter.setItems(itemsToUpdate)
                time = SystemClock.elapsedRealtime() + delayInMillis
                this.itemsToUpdate = null
            }
        }
        observer = Observer { items: X ->
            val t = SystemClock.elapsedRealtime()
            if (t >= time) {
                handler.removeCallbacks(updateTask)
                itemsToUpdate = items
                updateTask.run()
            } else {
                handler.removeCallbacks(updateTask)
                itemsToUpdate = items
                handler.postDelayed(updateTask, time - t)
            }
        }
    }
}