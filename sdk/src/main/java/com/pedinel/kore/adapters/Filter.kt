/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified by Pedro Parra 30/11/18 02:58 PM
 */
package com.pedinel.kore.adapters

interface Filter<T> {
    fun applies(item: T): Boolean
}