/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified by Pedro Parra 30/11/18 03:59 PM
 */
package com.pedinel.kore.adapters

import androidx.recyclerview.widget.DiffUtil

internal abstract class Observer<T> {
    abstract fun getItemId(item: T): Long
    abstract fun onFiltered(items: MutableList<T>): MutableList<T>
    abstract fun adapter(): ReactiveAdapter<T>
    abstract fun onDataSetChanged(oldItems: List<T>, newItems: List<T>)
    internal class BaseImpl<T>(private val adapter: Adapter<T, *>) : Observer<T>() {
        override fun adapter(): ReactiveAdapter<T> {
            return adapter
        }

        override fun getItemId(item: T): Long {
            return adapter.getItemId(item)
        }

        override fun onFiltered(items: MutableList<T>): MutableList<T> {
            return adapter.onFiltered(items)
        }

        override fun onDataSetChanged(oldItems: List<T>, newItems: List<T>) {
            adapter.notifyDataSetChanged()
        }
    }

    internal class RecyclerImpl<T>(private val adapter: RecyclerAdapter<T, *>) : Observer<T>() {
        override fun getItemId(item: T): Long {
            return adapter.getItemId(item)
        }

        override fun onFiltered(items: MutableList<T>): MutableList<T> {
            return adapter.onFiltered(items)
        }

        override fun adapter(): ReactiveAdapter<T> {
            return adapter
        }

        override fun onDataSetChanged(oldItems: List<T>, newItems: List<T>) {
            DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun getOldListSize(): Int {
                    return oldItems.size
                }

                override fun getNewListSize(): Int {
                    return newItems.size
                }

                override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                    val oldItem = oldItems[oldItemPosition]
                    val oldItemId = adapter.getItemId(oldItem)
                    if (oldItemId == -1L) return false
                    val newItem = newItems[newItemPosition]
                    return oldItemId == adapter.getItemId(newItem)
                }

                override fun areContentsTheSame(
                    oldItemPosition: Int,
                    newItemPosition: Int
                ): Boolean {
                    val oldItem = oldItems[oldItemPosition]
                    val itemLastUpdate = adapter.getItemLastUpdate(oldItem)
                    if (itemLastUpdate == -1L) return false
                    val newItem = newItems[newItemPosition]
                    return itemLastUpdate == adapter.getItemLastUpdate(newItem)
                }
            }).dispatchUpdatesTo(adapter)
            adapter.onContentUpdated(newItems)
        }
    }
}