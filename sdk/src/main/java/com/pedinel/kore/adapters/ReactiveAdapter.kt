/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 16/04/19 02:23 PM
 */
package com.pedinel.kore.adapters

import androidx.lifecycle.LiveData

interface ReactiveAdapter<T> {
    /**
     * Sets the items data source. This replaces any existing data source.
     *
     * @param source the data source
     * @param <X> the [Collection] subclass
    </X> */
    fun <X : Collection<T>> setSource(source: LiveData<X>?)

    /**
     * Updates the items list.
     *
     * @param newItems the new Item list. The [Adapter] will create a copy of the input
     * before updating the source.
     */
    fun setItems(newItems: Collection<T>)
    fun setFilter(filter: Filter<T>?)
    fun getFilter(): Filter<T>?
}