/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 11/06/19 10:24 AM
 */
package com.pedinel.kore.adapters

import android.content.Context
import android.graphics.Rect
import com.pedinel.kore.adapters.Utils.setLayoutAnimation
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import androidx.annotation.AnimRes
import androidx.lifecycle.LiveData
import androidx.annotation.CallSuper
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.databinding.ViewDataBinding
import com.pedinel.kore.viewmodels.ItemViewModel
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import android.util.TypedValue
import android.view.View
import com.pedinel.kore.adapters.Observer.RecyclerImpl

abstract class RecyclerAdapter<T, VH : RecyclerAdapter.ViewHolder<T>?> protected constructor(owner: LifecycleOwner) :
    RecyclerView.Adapter<VH>(), ReactiveAdapter<T> {
    private val itemPadding = ItemPadding()

    /**
     * Operation Handler delegated
     */
    private val delegated: AdapterHandler<T>
    private var contentListener: AdapterContentListener<T>? = null

    fun padding(): ItemPadding {
        return itemPadding
    }

    fun setContentListener(contentListener: AdapterContentListener<T>?) {
        this.contentListener = contentListener
    }

    override fun setFilter(filter: Filter<T>?) {
        delegated.setFilter(filter!!)
    }

    internal fun onContentUpdated(newItems: List<T>?) {
        if (contentListener != null) {
            contentListener!!.onAdapterContentChanged(this, newItems!!)
        }
    }

    protected val owner: LifecycleOwner
        get() = delegated.owner!!

    override fun getFilter(): Filter<T>? {
        return delegated.filter
    }

    open fun getItemId(item: T): Long {
        return -1
    }

    open fun getItemLastUpdate(item: T): Long {
        return -1
    }

    open fun onFiltered(items: MutableList<T>): MutableList<T> {
        return items
    }

    @AnimRes
    open fun onGetLayoutAnimation(): Int {
        return 0
    }

    override fun <X : Collection<T>> setSource(source: LiveData<X>?) {
        delegated.setSource(source)
    }

    override fun setItems(newItems: Collection<T>) {
        delegated.setItems(newItems)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        val item = delegated.getItemAt(position)
        holder!!.setItem(item)
    }

    override fun getItemId(position: Int): Long {
        return delegated.getItemId(position)
    }

    override fun getItemCount(): Int {
        return delegated.getItemCount()
    }

    protected fun getItemAt(position: Int): T {
        return delegated.getItemAt(position)
    }

    @CallSuper
    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        itemPadding.init(recyclerView.context)
        val layoutManager = recyclerView.layoutManager
        if (layoutManager != null) {
            if (layoutManager is GridLayoutManager) {
                val gridLayoutManager = layoutManager
                itemPadding.span = gridLayoutManager.spanCount
                itemPadding.horizontal =
                    gridLayoutManager.orientation == GridLayoutManager.HORIZONTAL
            } else {
                itemPadding.span = 1
                itemPadding.horizontal =
                    (layoutManager as LinearLayoutManager).orientation == LinearLayoutManager.HORIZONTAL
            }
        }
        setLayoutAnimation(recyclerView, onGetLayoutAnimation())
        recyclerView.addItemDecoration(itemPadding)
        recyclerView.scheduleLayoutAnimation()
    }

    @CallSuper
    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        recyclerView.removeItemDecoration(itemPadding)
        itemPadding.destroy()
    }

    abstract class ViewHolder<T> protected constructor(
        protected val binding: ViewDataBinding,
        protected val viewModel: ItemViewModel<T>,
        owner: LifecycleOwner?
    ) : RecyclerView.ViewHolder(
        binding.root
    ) {
        protected var lastItem: T? = null
        protected val context: Context
            protected get() = binding.root.context

        fun setItem(item: T) {
            if (lastItem !== item) {
                lastItem = item
                viewModel.setItem(item)
                onChanged(item)
            }
        }

        @CallSuper
        protected fun onChanged(item: T) {
            binding.invalidateAll()
        }

        init {
            binding.lifecycleOwner = owner
        }
    }

    class ItemPadding internal constructor() : ItemDecoration() {
        var horizontal = false

        /**
         * The Top Padding (over the first item)
         */
        private var top = 0

        /**
         * The Bottom Padding (below the last item)
         */
        private var bottom = 0

        /**
         * The Start Padding (before the first item)
         */
        private var left = 0

        /**
         * The End Padding (after the last item)
         */
        private var right = 0

        /**
         * The Inner Padding (between items)
         */
        private var midV = 0
        private var midH = 0
        var span = 0

        /**
         * The attached context
         */
        private var mContext: Context? = null

        /**
         * Changes the Top Padding (over the first item) in dp
         *
         * @param value the padding value
         */
        fun setTop(value: Int): ItemPadding {
            top = fromDP(mContext, value)
            return this
        }

        /**
         * Changes the Start Padding (before the first item) in dp
         *
         * @param value the padding value
         */
        fun setLeft(value: Int): ItemPadding {
            left = fromDP(mContext, value)
            return this
        }

        /**
         * Changes the Bottom Padding (below the last item) in dp
         *
         * @param value the padding value
         */
        fun setBottom(value: Int): ItemPadding {
            bottom = fromDP(mContext, value)
            return this
        }

        /**
         * Changes the End Padding (after the last item) in dp
         *
         * @param value the padding value
         */
        fun setRight(value: Int): ItemPadding {
            right = fromDP(mContext, value)
            return this
        }

        /**
         * Changes the Inner Padding (between items) in dp
         *
         * @param value the padding value
         */
        fun setMiddleHorizontal(value: Int): ItemPadding {
            midH = fromDP(mContext, value)
            return this
        }

        /**
         * Changes the Inner Padding (between items) in dp
         *
         * @param value the padding value
         */
        fun setMiddleVertical(value: Int): ItemPadding {
            midV = fromDP(mContext, value)
            return this
        }

        fun init(context: Context?) {
            mContext = context
        }

        fun destroy() {
            mContext = null
        }

        override fun getItemOffsets(
            outRect: Rect,
            view: View,
            parent: RecyclerView,
            state: RecyclerView.State
        ) {
            val pos = parent.getChildAdapterPosition(view)
            if (span == 1) {
                if (horizontal) {
                    outRect.left = if (pos == 0) left else midH
                    outRect.top = top
                    outRect.right = if (pos == state.itemCount - 1) right else midH
                    outRect.bottom = bottom
                } else {
                    outRect.left = left
                    outRect.top = if (pos == 0) top else midV
                    outRect.right = right
                    outRect.bottom = if (pos == state.itemCount - 1) bottom else midV
                }
            } else {
                if (horizontal) {
                    outRect.left = if (pos < span) left else midH
                    outRect.top = if (pos % span == 0) top else midV
                    outRect.right = if (pos / span >= (state.itemCount - 1) / span) right else midH
                    outRect.bottom = if (pos % span == span - 1) bottom else midV
                } else {
                    outRect.left = if (pos % span == 0) left else midH
                    outRect.top = if (pos < span) top else midV
                    outRect.right = if (pos % span == span - 1) right else midH
                    outRect.bottom =
                        if (pos / span >= (state.itemCount - 1) / span) bottom else midV
                }
            }
        }

        companion object {
            /**
             * Converts dp to pixels
             *
             * @param dp the DP
             * @return the pixels
             */
            private fun fromDP(context: Context?, dp: Int): Int {
                val displayMetrics = context!!.resources.displayMetrics
                val pixels = TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    dp.toFloat(),
                    displayMetrics
                )
                return pixels.toInt()
            }
        }
    }

    init {
        setHasStableIds(true)
        delegated = AdapterHandler(owner, RecyclerImpl(this))
    }
}