/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified by Pedro Parra 30/11/18 12:19 PM
 */
package com.pedinel.kore.adapters

import android.view.ViewGroup
import androidx.annotation.AnimRes
import android.view.animation.LayoutAnimationController
import android.view.animation.AnimationUtils

internal object Utils {
    @JvmStatic
    fun setLayoutAnimation(view: ViewGroup, @AnimRes animRes: Int) {
        if (animRes != 0) {
            val anim = AnimationUtils.loadLayoutAnimation(view.context, animRes)
            view.layoutAnimation = anim
        }
    }
}