/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 7/9/21 10:43 a. m.
 */

package com.pedinel.kore.exceptions

import com.pedinel.kore.http.ApiErrorResponse

/**
 * API error response
 */
class HttpException(val error: ApiErrorResponse) : Exception("HTTP Error") {
    override fun toString(): String {
        return "(code = ${error.responseCode}, payload = ${error.   payload})"
    }
}