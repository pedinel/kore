/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 29/04/19 02:18 PM
 */
package com.pedinel.kore.http

import androidx.annotation.CheckResult
import com.google.gson.JsonIOException
import com.google.gson.JsonSyntaxException
import okhttp3.Headers
import kotlin.reflect.KClass

/**
 * Response handler for API
 */
interface ApiResponse<T> {
    /**
     * @return the latest successful response. This value is kept if further response are
     * not successful
     */
    @CheckResult
    fun data(): T

    /**
     * @return The latest response headers, even if the request failed.
     */
    @CheckResult
    fun headers(): Headers?

    /**
     * Converts the response into another type. This only works if [hasError]
     * returns `true` Otherwise, this returns `null`
     *
     * @param clazz the response Class
     * @param <R> the Response Parameter type
     * @return the Parsed Response
     * @throws JsonIOException if there was a problem reading from the Reader
     * @throws JsonSyntaxException if json is not a valid representation for an object of type
    </R> */
    @CheckResult
    @Throws(JsonSyntaxException::class, JsonIOException::class)
    fun <R : ApiErrorResponse> error(clazz: KClass<R>): R

    /**
     * @return the latest error. This only works if the request could not be completed and throed
     * an exception. Otherwise, this returns `null`
     */
    @CheckResult
    fun exception(): Throwable?

    /**
     * Checks if the latest request was completed successfully. This means that the request was
     * completed with an status code of 200-299. Also, the [.data] was updated with the
     * latest result.
     *
     * @return `true` if the last request was completed successfully
     */
    val isSuccessful: Boolean

    /**
     * Checks if the latest request was completed but the status code is not 200-299. Any previous
     * [.data] is kept unchanged, but [.error] will retrieve a valid response.
     *
     * @return `true` if the last request was completed but the status code is not 200-299
     */
    fun hasError(): Boolean

    /**
     * Checks it the latest request could not be completed. If `true`, [.exception]
     * will retrieve the exception with the detailed error.
     *
     * @return `true` if the latest request could not be completed.
     */
    fun hasFailed(): Boolean

    /**
     * @return the Status code, or -1 if failed
     */
    fun code(): Int

    /**
     * @return `true` if the response were retrieved from cache.
     */
    val isCacheResponse: Boolean

    /**
     * @return the relative update time for the data
     */
    val updateTime: Long
}