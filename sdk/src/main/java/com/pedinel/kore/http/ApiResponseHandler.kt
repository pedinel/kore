/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 02/07/19 05:44 PM
 */
package com.pedinel.kore.http

import androidx.annotation.MainThread
import androidx.lifecycle.MutableLiveData
import com.pedinel.kore.utils.Logger
import okhttp3.Headers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.concurrent.atomic.AtomicInteger

/**
 * Triggers response events to listeners and interceptors
 */
internal class ApiResponseHandler<T>(
    private val mDelegated: MutableLiveData<ApiResponse<T>>,
    private val mInterceptor: (T) -> T,
    private val mListener: (T) -> Unit,
    private val mHttpListener: (Response<T>) -> Unit,
    private val mHeadersListener: (Headers) -> Unit
) : Callback<T> {

    @Volatile
    private var mWrapper: ApiResponseWrapper<T>? = null
    private val mAttemptsCount = AtomicInteger(0)

    fun execute(call: Call<T>, async: Boolean) {
        if (async) {
            mAttemptsCount.set(0)
            synchronized(this) { mDelegated.postValue(newResponse().setLoading()) }
            call.enqueue(this)
        } else {
            try {
                val response = call.execute()
                onResponse(call, response)
            } catch (e: IOException) {
                onFailure(call, e)
            }
        }
    }

    override fun onResponse(call: Call<T>?, response: Response<T>) {
        onResponse(response)
        synchronized(this) {
            mDelegated.postValue(
                newResponse().setResponse(
                    response,
                    mInterceptor,
                    mListener
                )
            )
        }
    }

    override fun onFailure(call: Call<T>, t: Throwable) {
        log.error(t, "Request Failed")
        if (mAttemptsCount.incrementAndGet() < MAX_ATTEMPTS) {
            call.clone().enqueue(this)
        } else {
            synchronized(this) { mDelegated.postValue(newResponse().setError(t)) }
        }
    }

    fun replaceData(newData: T) {
        synchronized(this) {
            val w = mWrapper
            if (w != null && w.isSuccessful) {
                mDelegated.postValue(w.setData(newData, mInterceptor, mListener))
            }
        }
    }

    fun response(): ApiResponse<T>? {
        return mWrapper
    }

    private fun newResponse(): ApiResponseWrapper<T> {
        val out: ApiResponseWrapper<T> = mWrapper?.copy() ?: ApiResponseWrapper()
        this.mWrapper = out
        return out
    }

    private fun onResponse(response: Response<T>) {
        mHttpListener.invoke(response)
        mHeadersListener.invoke(response.headers())
    }

    @MainThread
    fun finishLoad() {
        synchronized(this) {
            val wrapper = mWrapper
            if (wrapper != null && wrapper.isLoading()) {
                mDelegated.value = newResponse().setNotLoading()
            }
        }
    }

    companion object {
        private val log = Logger.getLogger(ApiResponseHandler::class)
        private const val MAX_ATTEMPTS = 1
    }
}