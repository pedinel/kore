/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 08/04/19 02:19 PM
 */
package com.pedinel.kore.http

import com.google.gson.JsonParseException
import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import java.io.IOException
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * DateTime format parser
 */
internal class HttpDateTimeAdapter(
    private val mDateFormat: String,
    private val mDateLocale: Locale,
    private val mTimeZone: TimeZone,
) : TypeAdapter<Date?>() {

    @Throws(IOException::class)
    override fun write(output: JsonWriter, date: Date?) {
        if (date == null) {
            output.nullValue()
        } else {
            val value: String = dateFormat().format(date)
            output.value(value)
        }
    }

    @Throws(IOException::class)
    override fun read(input: JsonReader): Date? {
        return try {
            when (input.peek()) {
                JsonToken.NULL -> {
                    input.nextNull()
                    null
                }
                else -> {
                    val date = input.nextString()
                    dateFormat().parse(date)
                }
            }
        } catch (e: ParseException) {
            throw JsonParseException(e)
        }
    }

    internal fun dateFormat(): DateFormat {
        val dateFormat: DateFormat = SimpleDateFormat(mDateFormat, mDateLocale)
        dateFormat.timeZone = mTimeZone
        return dateFormat
    }
}