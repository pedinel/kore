/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 27/06/19 05:45 PM
 */
package com.pedinel.kore.http

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import okhttp3.internal.toImmutableMap
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import java.util.*

/**
 * Maps [Map] objects from json objects
 */
internal class HttpMapAdapter : JsonDeserializer<Map<*, *>?> {
    @Throws(JsonParseException::class)
    override fun deserialize(
        json: JsonElement,
        typeOfT: Type,
        context: JsonDeserializationContext
    ): Map<*, *>? {
        if (!json.isJsonObject) {
            return LinkedHashMap<String, Any>()
        }
        val valueType: Type = if (typeOfT is ParameterizedType) {
            val actualTypeArguments = typeOfT.actualTypeArguments
            actualTypeArguments[1]
        } else {
            Any::class.java
        }
        val jsonAsObject = json.asJsonObject
        val out: MutableMap<String, *> = LinkedHashMap<String, Any>(jsonAsObject.size())
        for ((key, value) in jsonAsObject.entrySet()) {
            out[key] = context.deserialize(value, valueType)
        }
        return out.toImmutableMap()
    }
}