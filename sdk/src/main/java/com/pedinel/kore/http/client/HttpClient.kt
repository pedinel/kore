/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 26/8/21 4:23 p. m.
 */

package com.pedinel.kore

import android.content.Context
import com.pedinel.kore.http.client.HttpClientParams
import kotlin.reflect.KClass

/**
 * Http Client
 */
open class HttpClient {
    /**
     * Api object
     */
    internal lateinit var mService: HttpService

    /**
     * Resolves the singleton instance of [HttpService]
     */
    internal fun resolveApi(
        context: Context,
        params: HttpClientParams,
        setup: ((HttpClientBuilder) -> Unit)? = null
    ): HttpService {
        if (!::mService.isInitialized) {
            val appContext = context.applicationContext

            val builder = params.onCreateBuilder(appContext)
            setup?.invoke(builder)

            mService = builder.build(params)
        }

        return mService
    }

    companion object {
        /**
         * Keeps the singleton [HttpService] instance
         */
        internal lateinit var sInstance: HttpClient

        /**
         * Initializes
         */
        @JvmStatic fun initialize(context: Context): HttpClient {
            if (!::sInstance.isInitialized) {
                val appContext = context.applicationContext

                if (appContext is HttpClientParams) {
                    return initialize(context, appContext as HttpClientParams) {
                        appContext.onBuildHttpClient(it)
                    }
                } else {
                    throw RuntimeException("Application must implement HttpClientParams interface")
                }
            }

            return sInstance
        }

        @JvmStatic fun initialize(context: Context, parameters: HttpClientParams, setup: ((HttpClientBuilder) -> Unit)? = null): HttpClient {
            return initializer {
                HttpClient().apply {
                    resolveApi(context.applicationContext, parameters, setup)
                }
            }
        }

        private fun initializer(func: () -> HttpClient): HttpClient {
            if (!::sInstance.isInitialized) {
                sInstance = func.invoke()
            }

            return sInstance
        }

        @JvmStatic internal fun service(): HttpService {
            if (!::sInstance.isInitialized) {
                throw RuntimeException("HttpClient is not initialized")
            }

            return sInstance.mService
        }

        fun <T: Any> service(serviceClass: KClass<T>): T {
            return service().mProvider.getService(serviceClass)
        }
    }
}