/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 26/8/21 4:23 p. m.
 */

package com.pedinel.kore

import android.content.Context
import androidx.lifecycle.LiveData
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.pedinel.kore.http.ApiResponseFactory
import com.pedinel.kore.http.HttpMapAdapter
import com.pedinel.kore.http.HttpDateTimeAdapter
import com.pedinel.kore.http.HttpHeaderInterceptor
import com.pedinel.kore.lifecycle.LiveDataAdapter
import com.pedinel.kore.utils.Logger
import com.pedinel.kore.utils.Threads
import com.pedinel.kore.http.client.HttpClientParams
import okhttp3.Cache
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

open class HttpClientBuilder(context: Context) {
    private val mContext: Context = context.applicationContext
    private var mTestModeEnabled = false

    fun useTestingApi(testing: Boolean): HttpClientBuilder {
        mTestModeEnabled = testing
        return this
    }

    internal open fun build(params: HttpClientParams): HttpService {
        val gson = onBuildGson(params)
        val apiUrl = params.onGetHttpBaseUrl(mTestModeEnabled)

        log.debug("building API for $apiUrl")

        return HttpService(
            mContext = mContext,
            mGson = gson,
            mRetrofit = onBuildRetrofit(mContext, apiUrl, gson),
            mTestModeEnabled = mTestModeEnabled
        )
    }

    protected open fun onBuildGson(params: HttpClientParams): Gson {
        val singleLiveData: LiveDataAdapter.Single = LiveDataAdapter.Single()

        val builder = GsonBuilder()
            .registerTypeAdapter(LiveData::class.java, singleLiveData)
            .registerTypeAdapter(Map::class.java, HttpMapAdapter())
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)

        params.onGetDateTimeFormat()?.also {
            val adapter = HttpDateTimeAdapter(
                it.dateTimeFormat,
                it.locale,
                it.timeZone,
            )
            builder.registerTypeAdapter(Date::class.java, adapter)
        }

        params.onCustomizeGson(builder)

        return builder.create()
    }

    protected open fun onBuildRetrofit(context: Context, apiUrl: String, gson: Gson): Retrofit {
        return Retrofit.Builder()
            .baseUrl(apiUrl)
            .client(onBuildOkHttpClient(context))
            .callbackExecutor(Threads.threadExecutor)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(ApiResponseFactory())
            .build()
    }

    protected open fun onBuildOkHttpClient(context: Context): OkHttpClient {
        val httpCacheDirectory = File(context.filesDir, "http")
        val cacheSize = 8L * 1024 * 1024 // 8 MiB

        val builder = OkHttpClient.Builder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .dispatcher(Dispatcher(onBuildThreadExecutor()))
            .cache(Cache(httpCacheDirectory, cacheSize))

        if (mTestModeEnabled) {
            val logger = object : HttpLoggingInterceptor.Logger {
                override fun log(message: String) {
                    var segment = message
                    try {
                        if (segment.length <= 4000) {
                            log.debug(segment)
                            return
                        } else {
                            val v = segment.substring(0, 4000)
                            log.debug(v)
                        }
                        segment = segment.substring(4000)
                        log(segment)
                    } catch (e: OutOfMemoryError) {
                        log.debug("Out of memory. Can not display response")
                    }
                }
            }
            val interceptor = HttpLoggingInterceptor(logger)
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            builder.addInterceptor(interceptor)
        }

        onCustomizeOkHttpClient(builder)

        return builder.build()
    }

    protected open fun onCustomizeOkHttpClient(builder: OkHttpClient.Builder) {
        builder.addInterceptor(HttpHeaderInterceptor())
    }

    protected open fun onBuildThreadExecutor(): ExecutorService {
        return ThreadPoolExecutor(
            1, 2,
            60L, TimeUnit.SECONDS,
            LinkedBlockingQueue()
        )
    }

    companion object {
        private val log = Logger.getLogger(HttpClientBuilder::class)
    }
}