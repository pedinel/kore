/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 26/8/21 4:04 p. m.
 */

package com.pedinel.kore.http.client

import android.content.Context
import com.google.gson.GsonBuilder
import com.pedinel.kore.HttpClientBuilder
import java.util.*

interface HttpClientParams {

    fun onCreateBuilder(context: Context): HttpClientBuilder {
        return HttpClientBuilder(context)
    }

    fun onBuildHttpClient(builder: HttpClientBuilder) {
        //empty
    }

    fun onGetHttpBaseUrl(testModeEnabled: Boolean): String

    fun onGetDateTimeFormat(): HttpDateTimeConfig? {
//        return HttpDateTimeConfig(
//            mDateTimeFormat = "yyyy-MM-dd HH:mm:ss",
//            mLocale = Locale.ROOT,
//            mTimeZone = TimeZone.getTimeZone("UTC"),
//        )
        return null
    }

    fun onCustomizeGson(builder: GsonBuilder) {

    }
}