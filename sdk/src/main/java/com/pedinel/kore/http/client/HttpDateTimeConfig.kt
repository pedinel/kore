package com.pedinel.kore.http.client

import java.util.*

data class HttpDateTimeConfig(
    val dateTimeFormat: String,
    val locale: Locale = Locale.ROOT,
    val timeZone: TimeZone = TimeZone.getTimeZone("UTC"),
)