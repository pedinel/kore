/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 26/8/21 4:23 p. m.
 */

package com.pedinel.kore

import android.content.Context
import com.google.gson.Gson
import com.pedinel.kore.http.HttpServiceProvider
import com.pedinel.kore.utils.Logger
import retrofit2.Retrofit
import java.lang.ref.SoftReference
import kotlin.reflect.KClass

internal class HttpService(
    internal val mContext: Context,
    internal val mGson: Gson,
    internal val mRetrofit: Retrofit,
    internal val mTestModeEnabled: Boolean,
) {
    internal val mProvider = HttpServiceProvider(mRetrofit)
}