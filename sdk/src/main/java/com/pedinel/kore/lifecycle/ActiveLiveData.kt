/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 27/8/21 1:03 p. m.
 */

package com.pedinel.kore.lifecycle

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

/**
 * Self-initialized [LiveData] implementation
 */
internal class ActiveLiveData<Type>(
    private val task: (MutableLiveData<Type>) -> Unit
) : MutableLiveData<Type>() {

    override fun onActive() {
        super.onActive()

        task.invoke(this)
    }
}