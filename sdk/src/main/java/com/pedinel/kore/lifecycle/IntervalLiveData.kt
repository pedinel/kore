/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 27/8/21 1:03 p. m.
 */

package com.pedinel.kore.lifecycle

import android.os.SystemClock
import com.pedinel.kore.utils.TaskHandler

/**
 * LiveData that emits in regular intervals.
 */
internal class IntervalLiveData<Type>(
    private val intervalInMillis: Long,
) : LinkedLiveData<Type>() {
    private var intervalBeg = 0L
    private var lastUpdateTime = 0L
    private var running = false
    private var pendingValue: Type? = null

    private val action: Runnable = object : Runnable {
        override fun run() {
            if (!running) {
                return
            }

            val t = SystemClock.elapsedRealtime()
            var left: Long = intervalInMillis + lastUpdateTime - t

            if (left <= 0L) {
                setPendingValue()
                lastUpdateTime = t
                left = intervalInMillis

                if (intervalBeg == 0L) {
                    intervalBeg = t
                }
            }

//            if (intervalBeg != 0L) {
//                //smart adjust
//                left = intervalInMillis - ((t - intervalBeg) % intervalInMillis)
//            }

            handler.postDelayed(left)
        }

    }

    private val handler = TaskHandler(action)

    override fun onActive() {
        running = true
        intervalBeg = 0L

        super.onActive()
    }

    override fun onInactive() {
        running = false

        super.onInactive()

        handler.cancel()
    }

    override fun setValue(value: Type) {
        this.pendingValue = value
        handler.post()
    }

    private fun setPendingValue() {
        super.setValue(pendingValue)
    }
}