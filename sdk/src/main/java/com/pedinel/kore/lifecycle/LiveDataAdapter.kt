/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 23/04/19 01:21 PM
 */
package com.pedinel.kore.lifecycle

import androidx.lifecycle.LiveData
import com.google.gson.*
import com.google.gson.reflect.TypeToken
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

/**
 * Allows [LiveData] serialization with Gson.
 */
internal abstract class LiveDataAdapter<K : LiveData<*>?> : JsonSerializer<K>, JsonDeserializer<K> {
    @Throws(JsonParseException::class)
    override fun deserialize(
        json: JsonElement,
        typeOfT: Type,
        context: JsonDeserializationContext
    ): K {
        if (typeOfT is ParameterizedType) {
            val arguments = typeOfT.actualTypeArguments
            if (arguments.isNotEmpty()) {
                val typeOfArgument = arguments[0]
                return make(context.deserialize<Any>(json, typeOfArgument))
            }
        }
        return make(context.deserialize<Any>(json, GENERIC_TYPE))
    }

    override fun serialize(
        src: K,
        typeOfSrc: Type,
        context: JsonSerializationContext
    ): JsonElement {
        if (src == null) return JsonNull.INSTANCE
        val value: Any = src.value ?: return JsonNull.INSTANCE
        return context.serialize(value)
    }

    protected abstract fun make(value: Any?): K

    class Single : LiveDataAdapter<LiveData<*>>() {
        override fun make(value: Any?): LiveData<*> {
            return LiveDatas.from(value)
        }
    }

    companion object {
        private val GENERIC_TYPE: Type =
            object : TypeToken<kotlin.collections.Map<String?, String?>?>() {}.type
    }
}