/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 10/9/21 11:06 a. m.
 */

package com.pedinel.kore.lifecycle

interface Next<T> {
    fun value(emittedValue: T)

    fun value(): T?
}