/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 27/8/21 4:42 p. m.
 */
package com.pedinel.kore.lifecycle

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import com.pedinel.kore.utils.Threads

/**
 * Executes a task
 */
internal class TaskRunner(private val task: Runnable) {

    val STOPPED: Int = 0
    val LAUNCH_WHEN_STARTED: Int = 1
    val LAUNCH_NOW: Int = 2

    private var status = STOPPED

    @MainThread
    fun launch() {
        if (status == LAUNCH_NOW) {
            task.run()
        } else if (status == STOPPED) {
            status = LAUNCH_WHEN_STARTED
        }
    }

    @WorkerThread
    fun launchDelayed() {
        Threads.post { launch() }
    }

    @MainThread
    fun start() {
        if (status == 0) {
            status = LAUNCH_NOW
        } else if (status == LAUNCH_WHEN_STARTED) {
            status = LAUNCH_NOW
            task.run()
        }
    }

    internal companion object {
        fun of(async: Boolean, task: Runnable): TaskRunner {
            return if (async) {
                TaskRunner(task)
            } else {
                TaskRunner { Threads.executeAsync(task) }
            }
        }
    }
}