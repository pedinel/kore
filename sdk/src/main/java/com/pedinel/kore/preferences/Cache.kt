package com.pedinel.kore.preferences

import com.pedinel.kore.preferences.WrappedPreferences
import java.util.concurrent.ConcurrentHashMap

internal class Cache<T>(private val w: WrappedPreferences) {
    private val values: ConcurrentHashMap<String?, Any?> = ConcurrentHashMap()

    operator fun get(key: String?): T? {
        val v = values[key]
        return if (v !== nullValue) v as T else null
    }

    operator fun set(key: String?, value: T?): T? {
        values[key!!] = value ?: nullValue

        if (!w.mapper.containsKey(key)) {
            w.mapper[key] = this
        }

        return value
    }

    fun has(key: String?): Boolean {
        return values.containsKey(key)
    }

    fun remove(key: String?): T? {
        val v = values.remove(key)
        return if (v !== nullValue) v as T else null
    }

    fun clear() {
        values.clear()
    }

    companion object {
        private val nullValue = Any()
    }
}