package com.pedinel.kore.preferences

import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import androidx.annotation.CallSuper
import androidx.lifecycle.LiveData
import android.annotation.SuppressLint
import android.content.Context
import androidx.annotation.CheckResult
import androidx.annotation.WorkerThread
import kotlin.jvm.Synchronized
import com.pedinel.kore.preferences.WrappedPreferences
import com.pedinel.kore.utils.Threads
import java.util.HashSet

abstract class ConfigX protected constructor(prefName: String?, context: Context) {
    private val preferences: SharedPreferences
    private val listeners: HashSet<OnSharedPreferenceChangeListener?> = HashSet()

    @CallSuper
    fun save() {
        synchronized(this) {
            if (hasUnsavedChanges()) {
                edit().apply()
            }
        }
    }

    @CheckResult
    fun hasUnsavedChanges(): Boolean {
        synchronized(this) { return preferences.changed() }
    }

    fun registerOnSharedPreferenceChangeListener(l: OnSharedPreferenceChangeListener?) {
        synchronized(listeners) {
            if (!listeners.contains(l)) {
                listeners.add(l)
                preferences.registerOnSharedPreferenceChangeListener(l)
            }
        }
    }

    fun unregisterOnSharedPreferenceChangeListener(l: OnSharedPreferenceChangeListener?) {
        synchronized(listeners) {
            if (listeners.contains(l)) {
                listeners.remove(l)
                preferences.unregisterOnSharedPreferenceChangeListener(l)
            }
        }
    }

    @CheckResult
    fun <T> observable(key: String, defaultValue: T, type: Class<T>): LiveData<T> {
        return ObservableConfig(key, this, type, defaultValue)
    }

    protected val currentVersion: Int
        protected get() = 1

    @SuppressLint("CommitPrefEdits")
    @CheckResult
    @Synchronized
    protected fun edit(): Editor {
        return preferences.edit()
    }

    @CheckResult
    fun pref(): SharedPreferences {
        return preferences
    }

    @WorkerThread
    @Synchronized
    private fun onBoot() {
        val w = (pref() as WrappedPreferences).w
        val oldVersion = w.getInt("config_x_version", 1)
        val newVersion = currentVersion
        if (oldVersion != newVersion) {
            onVersionUpdated(oldVersion, newVersion)
            w.edit().putInt("config_x_version", newVersion).apply()
        }
    }

    @WorkerThread
    protected fun onVersionUpdated(oldVersion: Int, newVersion: Int) {
    }

    companion object {
        protected const val DEFAULT_PREF_NAME = "settings"
        private const val VERSION = "config_x_version"
    }

    init {
        var prefName = prefName
        if (prefName == null || prefName.length == 0) {
            prefName = "settings"
        }
        preferences = WrappedPreferences.load(context, prefName)
        Threads.executeAsync { onBoot() }
    }
}