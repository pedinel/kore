package com.pedinel.kore.preferences

import android.content.SharedPreferences

interface Editor : SharedPreferences.Editor {
    override fun putString(key: String, value: String?): Editor
    override fun putStringSet(key: String, values: Set<String>?): Editor
    fun putStringList(key: String, values: List<String?>?): Editor?
    override fun putInt(key: String, value: Int): Editor
    override fun putLong(key: String, value: Long): Editor
    override fun putFloat(key: String, value: Float): Editor
    override fun putBoolean(key: String, value: Boolean): Editor
    override fun remove(key: String): Editor
    override fun clear(): Editor
    fun hasChanges(): Boolean
}