package com.pedinel.kore.preferences

import android.content.SharedPreferences
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import androidx.lifecycle.LiveData
import com.pedinel.kore.utils.Threads

internal class ObservableConfig<T>(
    private val prefKey: String,
    private val configX: ConfigX,
    type: Class<T>,
    defaultValue: T
) : LiveData<T>() {
    private val listener: OnSharedPreferenceChangeListener
    private val type: Class<T>
    private val defaultValue: T
    private var validate = false

    @get:Synchronized
    private var delegated: (() -> T?)? = null
        get() {
            if (field == null) {
                field = when (type) {
                    Int::class.java -> {
                        { intValue() }
                    }
                    Long::class.java -> {
                        { longValue() }
                    }
                    Float::class.java -> {
                        { floatValue() }
                    }
                    String::class.java -> {
                        { stringValue() }
                    }
                    Boolean::class.java -> {
                        { booleanValue() }
                    }
                    else -> {
                        require(type == MutableSet::class.java) { "type " + type + " not allowed" }
                        ({ stringSetValue() })
                    }
                }
            }
            return field
        }

    override fun onActive() {
        super.onActive()
        update()
        configX.registerOnSharedPreferenceChangeListener(listener)
    }

    override fun onInactive() {
        super.onInactive()
        configX.unregisterOnSharedPreferenceChangeListener(listener)
    }

    private fun update() {
        Threads.queueLatestAsync(this) {
            val value = delegated!!.invoke()
            postValue(value)
        }
    }

    override fun setValue(newValue: T?) {
        if (validate) {
            val oldValue = value
            if (newValue === oldValue || newValue != null && newValue == oldValue) {
                return
            }
        } else {
            validate = true
        }
        super.setValue(newValue)
    }

    private fun intValue(): T? {
        return type.cast(configX.pref().getInt(prefKey, (defaultValue as Int)))
    }

    private fun longValue(): T? {
        return type.cast(configX.pref().getLong(prefKey, (defaultValue as Long)))
    }

    private fun floatValue(): T? {
        return type.cast(configX.pref().getFloat(prefKey, (defaultValue as Float)))
    }

    private fun stringValue(): T? {
        return type.cast(configX.pref().getString(prefKey, defaultValue as String))
    }

    private fun booleanValue(): T? {
        return type.cast(configX.pref().getBoolean(prefKey, (defaultValue as Boolean)))
    }

    private fun stringSetValue(): T? {
        return type.cast(configX.pref().getStringSet(prefKey, defaultValue as Set<String>))
    }

    override fun toString(): String {
        return prefKey + " = " + this.value
    }

    private interface Delegated<T> {
        fun execute(): T
    }

    init {
        listener =
            OnSharedPreferenceChangeListener { sharedPreferences: SharedPreferences?, key: String ->
                if (prefKey == key) {
                    value = delegated!!.invoke()
                }
            }
        this.type = type
        this.defaultValue = defaultValue
    }
}