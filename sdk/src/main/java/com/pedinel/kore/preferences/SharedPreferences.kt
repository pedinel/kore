package com.pedinel.kore.preferences

import android.content.SharedPreferences

interface SharedPreferences : SharedPreferences {
    fun getStringList(key: String?, defValues: List<String?>?): List<String?>?
    override fun edit(): Editor
    fun changed(): Boolean
}