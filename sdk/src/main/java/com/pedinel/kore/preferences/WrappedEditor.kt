package com.pedinel.kore.preferences

import android.content.SharedPreferences
import kotlin.jvm.Volatile
import org.json.JSONArray
import kotlin.jvm.Synchronized

internal class WrappedEditor(internal val pref: WrappedPreferences) : Editor {
    @Volatile
    private var w: SharedPreferences.Editor? = null

    private fun edit(): SharedPreferences.Editor? {
        if (w == null) {
            w = pref.w.edit()
        }
        return w
    }

    override fun putString(key: String, value: String?): Editor {
        pref.stringCache[key] = value
        edit()!!.putString(key, value)
        return this
    }

    override fun putStringSet(key: String, values: Set<String>?): Editor {
        pref.stringSetCache[key] = values
        edit()!!.putStringSet(key, values)
        return this
    }

    override fun putStringList(key: String, values: List<String?>?): Editor {
        putString(key, JSONArray(values).toString())
        return this
    }

    override fun putInt(key: String, value: Int): Editor {
        pref.integerCache[key] = value
        edit()!!.putInt(key, value)
        return this
    }

    override fun putLong(key: String, value: Long): Editor {
        pref.longCache[key] = value
        edit()!!.putLong(key, value)
        return this
    }

    override fun putFloat(key: String, value: Float): Editor {
        pref.floatCache[key] = value
        edit()!!.putFloat(key, value)
        return this
    }

    override fun putBoolean(key: String, value: Boolean): Editor {
        pref.booleanCache[key] = value
        edit()!!.putBoolean(key, value)
        return this
    }

    override fun remove(key: String): Editor {
        pref.mapper.remove(key)?.remove(key)
        edit()!!.remove(key)
        return this
    }

    override fun clear(): Editor {
        val var1: Iterator<*> = pref.mapper.values.iterator()
        while (var1.hasNext()) {
            val cache = var1.next() as Cache<*>
            cache.clear()
        }
        pref.mapper.clear()
        edit()!!.clear()
        return this
    }

    override fun hasChanges(): Boolean {
        return w != null
    }

    @Synchronized
    override fun commit(): Boolean {
        return if (w != null && w!!.commit()) {
            w = null
            true
        } else {
            false
        }
    }

    @Synchronized
    override fun apply() {
        if (w != null) {
            w!!.apply()
            w = null
        }
    }
}