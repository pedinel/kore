package com.pedinel.kore.preferences

import android.content.Context
import org.json.JSONArray
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import java.lang.RuntimeException
import java.util.ArrayList
import java.util.HashMap
import java.util.concurrent.ConcurrentHashMap

internal class WrappedPreferences private constructor(internal val w: android.content.SharedPreferences) :
    SharedPreferences {
    val stringCache: Cache<String?> = Cache(this)
    val integerCache: Cache<Int?> = Cache(this)
    val floatCache: Cache<Float?> = Cache(this)
    val stringSetCache: Cache<Set<String>?> = Cache(this)
    val longCache: Cache<Long?> = Cache(this)
    val booleanCache: Cache<Boolean?> = Cache(this)
    val mapper: ConcurrentHashMap<String?, Cache<*>?> = ConcurrentHashMap()

    @Volatile
    private var editor: Editor? = null

    override fun getAll(): Map<String, *> {
        return w.all
    }

    override fun getString(key: String, defValue: String?): String? {
        return if (stringCache.has(key)) stringCache[key] else stringCache.set(
            key,
            w.getString(key, defValue)
        )
    }

    override fun getStringSet(key: String, defValues: Set<String>?): Set<String>? {
        return if (stringSetCache.has(key)) stringSetCache[key] else stringSetCache.set(
            key,
            w.getStringSet(key, defValues)
        )
    }

    override fun getStringList(key: String?, defValues: List<String?>?): List<String?> {
        val v = if (stringCache.has(key)) stringCache[key] else stringCache.set(
            key,
            w.getString(key, if (defValues != null) JSONArray(defValues).toString() else "[]")
        )
        return try {
            val jsonArray = JSONArray(v)
            val o: ArrayList<String?> = ArrayList(jsonArray.length())

            o.apply {
                for (i in 0 until jsonArray.length()) {
                    add(jsonArray.getString(i))
                }
            }
        } catch (var7: Throwable) {
            throw RuntimeException("Invalid Json $v", var7)
        }
    }

    override fun getInt(key: String, defValue: Int): Int {
        return if (integerCache.has(key)) integerCache[key] as Int else integerCache.set(
            key,
            w.getInt(key, defValue)
        )!!
    }

    override fun getLong(key: String, defValue: Long): Long {
        return if (longCache.has(key)) longCache[key] as Long else longCache.set(
            key,
            w.getLong(key, defValue)
        )!!
    }

    override fun getFloat(key: String, defValue: Float): Float {
        return if (floatCache.has(key)) floatCache[key] as Float else floatCache.set(
            key,
            w.getFloat(key, defValue)
        )!!
    }

    override fun getBoolean(key: String, defValue: Boolean): Boolean {
        return if (booleanCache.has(key)) booleanCache[key] as Boolean else booleanCache.set(
            key,
            w.getBoolean(key, defValue)
        )!!
    }

    override fun contains(key: String): Boolean {
        return w.contains(key)
    }

    override fun edit(): Editor {
        return getEditor()!!
    }

    override fun changed(): Boolean {
        synchronized(this) {
            return editor.let {
                it?.hasChanges() ?: false
            }
        }
    }

    override fun registerOnSharedPreferenceChangeListener(listener: OnSharedPreferenceChangeListener) {
        w.registerOnSharedPreferenceChangeListener(listener)
    }

    override fun unregisterOnSharedPreferenceChangeListener(listener: OnSharedPreferenceChangeListener) {
        w.unregisterOnSharedPreferenceChangeListener(listener)
    }

    private fun getEditor(): Editor? {
        synchronized(this) {
            if (editor == null) {
                editor = WrappedEditor(this)
            }
        }

        return editor
    }

    companion object {
        private val loadedPreferences: HashMap<String?, SharedPreferences?> = HashMap()

        fun load(context: Context, prefName: String?): SharedPreferences {
            var loaded = loadedPreferences[prefName]
            if (loaded == null) {
                loaded = WrappedPreferences(context.getSharedPreferences(prefName, 0))
                loadedPreferences[prefName] = loaded
            }
            return loaded
        }
    }
}