/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 27/8/21 12:24 p. m.
 */
package com.pedinel.kore.utils

import android.os.Handler
import android.os.Looper
import androidx.annotation.CheckResult
import com.pedinel.kore.utils.Logger
import java.util.concurrent.*
import java.util.concurrent.atomic.AtomicInteger
import kotlin.math.max

/**
 * Threads Handler and executor
 */
object Threads {

    private const val CORE_POOL_SIZE = 1
    private const val MAXIMUM_POOL_SIZE = 20
    private const val BACKUP_POOL_SIZE = 5
    private const val KEEP_ALIVE_SECONDS = 3L

    private val logger = Logger.getLogger(Threads::class)
    private val mainLoopHandler: Handler = Handler(Looper.getMainLooper())
    private val serialHandler = SerialDelegatedHandler()
    internal val threadExecutor: Executor

    init {
        val threadFactory: ThreadFactory = object : ThreadFactory {
            private val mCount = AtomicInteger(1)
            override fun newThread(r: Runnable): Thread {
                return Thread(r, "PaycoThread #" + mCount.getAndIncrement())
            }
        }

        var backupExecutor: ThreadPoolExecutor? = null
        var backupExecutorQueue: LinkedBlockingQueue<Runnable>?

        val runOnSerialPolicy: RejectedExecutionHandler = object : RejectedExecutionHandler {
            override fun rejectedExecution(r: Runnable, e: ThreadPoolExecutor) {
                logger.warn("Exceeded ThreadPoolExecutor pool size")
                // As a last ditch fallback, run it on an executor with an unbounded queue.
                // Create this executor lazily, hopefully almost never.
                synchronized(this) {
                    if (backupExecutor == null) {
                        backupExecutorQueue =
                            LinkedBlockingQueue<Runnable>()
                        backupExecutor = ThreadPoolExecutor(
                            BACKUP_POOL_SIZE,
                            BACKUP_POOL_SIZE,
                            KEEP_ALIVE_SECONDS,
                            TimeUnit.SECONDS,
                            backupExecutorQueue,
                            threadFactory
                        )
                        backupExecutor!!.allowCoreThreadTimeOut(true)
                    }
                }
                backupExecutor!!.execute(r)
            }
        }

        val threadPoolExecutor = ThreadPoolExecutor(
            CORE_POOL_SIZE, MAXIMUM_POOL_SIZE, KEEP_ALIVE_SECONDS, TimeUnit.SECONDS,
            SynchronousQueue(), threadFactory)

        threadPoolExecutor.rejectedExecutionHandler = runOnSerialPolicy
        threadExecutor = threadPoolExecutor
    }

    /**
     * Executes a Task in the Main Thread
     *
     * @param runnable the Task
     */
    fun post(runnable: Runnable) {
        mainLoopHandler.post(runnable)
    }

    /**
     * Executes a Task in Main Thread after the given delay (in milliseconds)
     *
     * @param milliseconds the delay
     * @param runnable the Task
     */
    fun postDelayed(milliseconds: Long, runnable: Runnable) {
        mainLoopHandler.postDelayed(runnable, milliseconds)
    }

    /**
     * Executes a Task in a Worker Thread
     *
     * @param runnable the Task
     */
    fun executeAsync(runnable: Runnable) {
        threadExecutor.execute(runnable)
    }

    /**
     * Executes a Task in a Worker Thread. The task that shares the same delegated
     * are executed one before the another
     *
     * @param delegated the delegated
     * @param runnable the Task
     */
    fun queueAsync(delegated: Any, runnable: Runnable) {
        serialHandler.execute(delegated, runnable)
    }

    /**
     * Executes a Task in a Worker Thread. The task that shares the same delegated
     * are executed one before the another
     *
     * @param delegated the delegated
     * @param runnable the Task
     */
    fun queueLatestAsync(delegated: Any, runnable: Runnable) {
        serialHandler.executeLast(delegated, runnable)
    }

    /**
     * Executes a Task in Main Thread after the given delay (in milliseconds)
     *
     * @param milliseconds the delay
     * @param task the Task
     *
     * @return an [DelayedTask] object that can be cancelled or updated
     */
    @CheckResult
    fun delayedTask(milliseconds: Long, task: Runnable): DelayedTask {
        val out = DelayedTask(task, false)
        out.setDelay(milliseconds)
        return out
    }

    /**
     * Executes a Task in Worker Thread after the given delay (in milliseconds)
     *
     * @param milliseconds the delay
     * @param task the Task
     *
     * @return an [DelayedTask] object that can be cancelled or updated
     */
    @CheckResult
    fun delayedAsyncTask(milliseconds: Long, task: Runnable): DelayedTask {
        val out = DelayedTask(task, true)
        out.setDelay(milliseconds)
        return out
    }

    @CheckResult
    fun <Type> chain(task: () -> Type): ChainHandler<Type> {
        return ChainHandler(task, false)
    }

    @CheckResult
    fun <Type> chainAsync(task: () -> Type): ChainHandler<Type> {
        return ChainHandler(task, true)
    }

    private fun executeInternal(async: Boolean, runnable: Runnable) {
        if (async) {
            executeAsync(runnable)
        } else {
            runnable.run()
        }
    }

    /**
     * A Task Delayer Manager that executes a Task when the specified delay elapses.
     * The task is executed only once
     */
    class DelayedTask internal constructor(
        @field:Volatile private var task: Runnable?,
        async: Boolean
    ) {
        private val taskWrapper = object : Runnable {
            override fun run() {
                synchronized(this@DelayedTask) {
                    if (isDone || isCancelled) return
                    isDone = true
                }
                val delayedTask = task
                if (delayedTask != null) {
                    task = null
                    executeInternal(async, delayedTask)
                }
            }
        }

        /**
         * Checks if the task was executed. If cancelled, this will return false.
         *
         * @return `true` only if the task was successfully executed.
         */
        @field:Volatile
        var isDone = false

        /**
         * Checks if the task was cancelled. If executed, this will return false.
         *
         * @return `true` only if the task was cancelled before execution.
         */
        @field:Volatile
        var isCancelled = false
            private set

        /**
         * Replaces the delay with the given one. This starts over the delay if not executed yet.
         *
         * @param milliseconds the new delay
         */
        fun setDelay(milliseconds: Long) {
            synchronized(this@DelayedTask) {
                if (isDone || isCancelled) return
                mainLoopHandler.removeCallbacks(taskWrapper)
                mainLoopHandler.postDelayed(taskWrapper, max(0, milliseconds))
            }
        }

        /**
         * Cancels (aborts) the task. This task will never be executed after this.
         */
        fun cancel() {
            synchronized(this@DelayedTask) {
                if (isDone || isCancelled) return
                isCancelled = true
                mainLoopHandler.removeCallbacks(taskWrapper)
            }
        }

        /**
         * Checks if the task has not been executed or cancelled.
         *
         * @return `true` only if the task has not been executed and has not been cancelled.
         */
        val isPending: Boolean
            get() = !isDone && !isCancelled
    }

    internal class SerialDelegatedHandler {
        private val map: MutableMap<Any?, ChainedExecutor?> = ConcurrentHashMap()

        fun execute(delegated: Any, task: Runnable) {
            synchronized(map) {
                var executor = map[delegated]
                if (executor != null) {
                    executor.chain(task)
                    return
                }
                executor = ChainedExecutor(delegated, task)
                map[delegated] = executor
                executor.execute()
            }
        }

        fun executeLast(delegated: Any, task: Runnable) {
            synchronized(map) {
                var executor = map[delegated]
                if (executor != null) {
                    executor.setNext(task)
                    return
                }
                executor = ChainedExecutor(delegated, task)
                map[delegated] = executor
                executor.execute()
            }
        }

        internal inner class ChainedExecutor(
            private val delegated: Any,
            private val task: Runnable
        ) : Runnable {
            private var next: ChainedExecutor? = null

            fun execute() {
                mainLoopHandler.post { threadExecutor.execute(this) }
            }

            fun chain(task: Runnable) {
                if (next == null) {
                    next = ChainedExecutor(
                        delegated, task
                    )
                } else {
                    next!!.chain(task)
                }
            }

            fun setNext(task: Runnable) {
                next = ChainedExecutor(
                    delegated, task
                )
            }

            override fun run() {
                task.run()
                synchronized(map) {
                    if (next == null) {
                        map.remove(delegated)
                        return
                    }
                    map[delegated] = next
                    next!!.execute()
                }
            }
        }
    }

    class ChainHandler<Type> {
        private val executor: ((Type?) -> Unit) -> Unit

        internal constructor (task: () -> Type?, async: Boolean) {
            executor = { next ->
                executeInternal(async) {
                    val result = task.invoke()
                    post { next.invoke(result) }
                }
            }
        }

        internal constructor(action: ((Type?) -> Unit) -> Unit) {
            executor = action
        }

        @CheckResult
        private fun <Output> then(async: Boolean, task: (Type?) -> Output): ChainHandler<Output> {
            return ChainHandler { next ->
                executor.invoke {
                    executeInternal(async) {
                        val result = task.invoke(it)
                        post { next.invoke(result) }
                    }
                }
            }
        }

        /**
         * Executes a new task in main thread that receives the result from previous chain link
         */
        @CheckResult
        fun <Output> chain(task: (Type?) -> Output): ChainHandler<Output> {
            return then(false, task)
        }

        /**
         * Executes a new task in background, that receives the result from previous chain link
         */
        @CheckResult
        fun <Output> chainAsync(task: (Type?) -> Output): ChainHandler<Output> {
            return then(true, task)
        }

        /**
         * Finishes the chain using a result callback in main thread
         */
        fun execute(task: (Type?) -> Unit) {
            post {
                executor.invoke {
                    task.invoke(it)
                }
            }
        }

    }
}