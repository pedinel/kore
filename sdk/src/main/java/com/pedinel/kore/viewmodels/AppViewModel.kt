/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 19/11/18 09:39 AM
 */
package com.pedinel.kore.viewmodels

import android.app.Application
import androidx.annotation.CallSuper
import androidx.lifecycle.AndroidViewModel
import com.pedinel.kore.http.ApiRequest
import com.pedinel.kore.utils.Threads
import java.util.*

abstract class AppViewModel protected constructor(application: Application) : AndroidViewModel(application) {
    private val requestList: MutableList<ApiRequest<*>> = Vector()
    private var mWatcher: RequestWatcher? = null

    fun setWatcher(watcher: RequestWatcher) {
        mWatcher = watcher
    }

    protected fun requestCreated(request: ApiRequest<*>) {
        Threads.post {
            requestList.add(request)
            mWatcher?.onRequestCreated(request)
        }
    }

    @CallSuper
    override fun onCleared() {
        mWatcher = null
        requestList.forEach {
            it.close()
        }
        requestList.clear()
    }
}