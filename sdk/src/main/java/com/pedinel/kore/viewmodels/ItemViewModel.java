/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 17/05/19 11:40 AM
 */

package com.pedinel.kore.viewmodels;

import androidx.annotation.CallSuper;
import androidx.annotation.MainThread;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

@SuppressWarnings("unused")
public abstract class ItemViewModel<T> extends ViewModel {
    protected final MediatorLiveData<T> mSource = new MediatorLiveData<>();

    public LiveData<T> getItem() {
        return mSource;
    }

    @CallSuper
    @MainThread
    public void setItem(T item) {
        if (item != null) {
            this.mSource.setValue(item);
        }
    }

    /**
     * Links a LiveData changes to automatically update this item ViewModel
     *
     * @param source the source where the data will be retrieved
     */
    public final void setSource(LiveData<? extends T> source) {
        mSource.addSource(source, this::setItem);
    }
}
